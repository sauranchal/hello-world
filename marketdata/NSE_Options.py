import requests
import pandas as pd
from bs4 import BeautifulSoup
import logging
import time
import datetime
import json
from flask import Flask
from flask import Response
from flask_cache import Cache

app = Flask(__name__)
#cache = Cache(app, config={'CACHE_TYPE': 'redis','CACHE_REDIS_HOST':'dq-md-poc.aejypb.ng.0001.aps1.cache.amazonaws.com'})
#cache = Cache(app, config={'CACHE_TYPE': 'simple','CACHE_DEFAULT_TIMEOUT': 10})
cache = Cache(app, config={'CACHE_TYPE': 'filesystem', 'CACHE_DIR': '/tmp'})

def getCurrentTime():
    #return datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    #return time.time()
    return str(int(round(time.time() * 1000)))


def getOptionChainCallPutFromCache(symbol, expiryDate):
    #cache.init_app(app, config={'CACHE_TYPE': 'simple','CACHE_REDIS_HOST':'localhost'})
    rv=cache.get(symbol+expiryDate)
    if rv is None:
      rv = getOptionChainCallPut(symbol, expiryDate)
      cache.set(symbol+expiryDate, rv)
    return rv

def getOptionChain(symbol, expiryDate):
    max_retries = 5
    retry = 0
    #cache.set()
    while (retry < max_retries):

        try:

            if symbol == 'NIFTY' or symbol == 'BANKNIFTY':
                instrumentType = 'OPTIDX'
            else:
                instrumentType = 'OPTSTK'

            Base_url = ("https://www.nseindia.com/live_market/dynaContent/" +
                        "live_watch/option_chain/optionKeys.jsp?symbol="+symbol+"&" +
                        "instrument="+instrumentType+"&date="+expiryDate)
            print(Base_url)
            page = requests.get(Base_url)
            page.status_code
            page.content

            soup = BeautifulSoup(page.content, 'html.parser')
            #print(soup.prettify())

            table_it = soup.find_all(class_="opttbldata")
            table_cls_1 = soup.find_all(id="octable")

            col_list = []

            # The code given below will pull the headers of the Option Chain table
            for mytable in table_cls_1:
                table_head = mytable.find('thead')

                try:
                    rows = table_head.find_all('tr')
                    for tr in rows:
                        cols = tr.find_all('th')
                        for th in cols:
                            er = th.text
                            ee = er.encode('utf8')
                            ee = str(ee, 'utf-8')
                            col_list.append(ee)

                except:
                    print("no thead")

            col_list_fnl = [e for e in col_list if e not in ('CALLS', 'PUTS', 'Chart', '\xc2\xa0', '\xa0')]

            #print(col_list_fnl)

            table_cls_2 = soup.find(id="octable")
            all_trs = table_cls_2.find_all('tr')
            req_row = table_cls_2.find_all('tr')

            new_table = pd.DataFrame(index=range(0, len(req_row) - 3), columns=col_list_fnl)

            row_marker = 0

            for row_number, tr_nos in enumerate(req_row):

                # This ensures that we use only the rows with values
                if row_number <= 1 or row_number == len(req_row) - 1:
                    continue

                td_columns = tr_nos.find_all('td')

                # This removes the graphs columns
                select_cols = td_columns[1:22]
                cols_horizontal = range(0, len(select_cols))

                for nu, column in enumerate(select_cols):
                    utf_string = column.get_text()
                    utf_string = utf_string.strip('\n\r\t": ')

                    tr = utf_string.encode('utf-8')
                    tr = str(tr, 'utf-8')
                    tr = tr.replace(',', '')
                    new_table.ix[row_marker, [nu]] = tr

                row_marker += 1
            break
        except Exception as exception:
            print('Exception occurred::', exception)
            logging.info('Exception occurred in fetching option chain::' + str(exception))
            # retrying in case of failures
            time.sleep(2)
            retry = retry + 1
            if retry == max_retries:
                raise Exception('Unable to Fetch Option Chain from NSE.')

    #print(new_table)
    return new_table


def getOptionChainCallPut(symbol, expiryDate):
    df = getOptionChain(symbol, expiryDate)

    index_strike_price = df.columns.get_loc('Strike Price')

    df_call = df.iloc[:, :index_strike_price + 1]
    df_puts = df.iloc[:, index_strike_price:]

    #print(df_call)
    #print(df_puts)
    return df_call, df_puts


def getOptionsMargin(symbol, expiryMonth, optionType, strikePrice, qty, tradeType):
    max_retries = 5
    retry = 0

    marginInfo = 0

    while (retry < max_retries):

        try:

            ZERODHA_URL = "https://zerodha.com/margin-calculator/SPAN/"

            payload = {'action': 'calculate',
                       'exchange[]': 'NFO',
                       'product[]': 'OPT',
                       'scrip[]': symbol+expiryMonth,
                       'option_type[]': optionType,
                       'strike_price[]': strikePrice,
                       'qty[]': qty,
                       'trade[]': tradeType}
            r = requests.post(ZERODHA_URL, data=payload)

            marginJson = r.text
            marginDict = json.loads(marginJson)
            marginInfo = marginDict['total']['total']
            #print(marginDict['total']['total'])
            break
        except Exception as exception:
            print('Exception occurred::', exception)
            logging.info('Exception occurred in fetching option chain::' + str(exception))
            # retrying in case of failures
            time.sleep(2)
            retry = retry + 1
            if retry == max_retries:
                raise Exception('Unable to Margin Info from Zerodha.')

    return marginInfo

@app.route("/")
def hello():
    return "<h1 style='color:Green'>Hello  Theree!</h1>"

@app.route("/option-chain/call-put")
def getOptionChainCallPutRest():
    symbol= 'NIFTY'
    expiryDate = '31JAN2019'
    print('Print1: '+getCurrentTime())
    df_call, df_puts=getOptionChainCallPutFromCache(symbol, expiryDate)
    print('Print2: ' + getCurrentTime())
    #chain=df_call.reset_index()
    chain=df_call.to_json()
    print('Print3: ' + getCurrentTime())
    pd1=Response(chain, mimetype='application/json')
    #print(pd.read_json(pd1.content))
    print('Print4: ' + getCurrentTime())
    return pd1

    #getOptionChain
    #return getOptionChain(symbol, expiryDate)

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000, debug=True)

    #getOptionChainCallPut
    #getOptionChainCallPut(symbol,expiryDate)

    #symbol, expiryMonth, optionType, strikePrice, qty, tradeType = 'NIFTY','18DEC','CE','10400','75','sell'
    #getOptionsMargin(symbol, expiryMonth, optionType, strikePrice, qty, tradeType)*/
