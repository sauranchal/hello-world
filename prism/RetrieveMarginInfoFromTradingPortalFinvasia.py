
## Python Script to automate upstox token refresh
from __future__ import print_function # Python 2/3 compatibility
from selenium import webdriver
import time
from datetime import datetime
import pytz
import logging
import os

def scrapPrismFinvasia():
        #get log file name from relative path using os
        log_file_name = os.path.splitext(os.path.realpath(__file__))[0]+'.log'
        #get current date with IST time zone
        date_time = datetime.now(pytz.timezone('Asia/Kolkata')).strftime('%d-%m-%Y %H:%M:%S')
        #logging config
        logging.basicConfig(filename=log_file_name,format=date_time+' %(message)s',level=logging.INFO)
        url="https://trade.finvasia.com/"
        logging.info("Login URL: "+url)
        logging.info(time.time())

        #Adding Chrome options
        options = webdriver.ChromeOptions()
        options.binary_location = '/opt/google/chrome/google-chrome' #comment this line for running on local
        options.add_argument('--headless')
        options.add_argument('--no-sandbox') # this is important for running headless on ec2
        service_log_path = "{}/chromedriver.log".format("/tmp")
        service_args = ['--verbose']
        driver = webdriver.Chrome('/usr/bin/chromedriver', # give your chromedriver path here
                chrome_options=options,
                service_args=service_args,
                service_log_path=service_log_path)

        driver.get(url)
        driver.find_element_by_id("inputEmail").send_keys("#####")
        driver.find_element_by_id("inputPassword").send_keys("#####")
        driver.find_element_by_id("loginContainer").find_element_by_tag_name("button").send_keys("u'\ue007'")
        time.sleep(2)
        driver.switch_to.parent_frame()
        driver.find_element_by_xpath("/html/body/div[9]/div/div[10]/button[1]").send_keys("u'\ue007'")
        driver.find_element_by_xpath("//p[contains(text(),'(1)')]/parent::div/input").send_keys("#####")
        driver.find_element_by_xpath("//p[contains(text(),'(2)')]/parent::div/input").send_keys("#####")
        driver.find_elements_by_xpath("//button[1][contains(text(),'Go')]").__getitem__(1).click()
        time.sleep(5)
        balance=driver.find_element_by_xpath("//div[@class='list-inline fleft'][1]/span[2]").text
        margin=driver.find_element_by_xpath("//div[@class='list-inline fleft'][2]/span[2]").text
        freemargin=driver.find_element_by_xpath("//div[@class='list-inline fleft'][3]/span[2]").text
        pnl=driver.find_element_by_xpath("//div[@class='list-inline fleft'][4]/span[2]").text
        driver.find_element_by_xpath("//a[@logout='']").click()
        trade_vals={"balance":balance,"margin":margin,"freemargin":freemargin,"pnl":pnl}
        logging.info(trade_vals)
        logging.info(time.time())
        driver.close()
        return trade_vals;

if __name__ == "__main__":
        scrapPrismFinvasia()