
## Python Script to automate upstox token refresh
from __future__ import print_function # Python 2/3 compatibility
from selenium import webdriver
import time
from datetime import datetime
import pytz
import logging
import os

def scrapPrismFinvasia():
        #get log file name from relative path using os
        log_file_name = os.path.splitext(os.path.realpath(__file__))[0]+'.log'
        #get current date with IST time zone
        date_time = datetime.now(pytz.timezone('Asia/Kolkata')).strftime('%d-%m-%Y %H:%M:%S')
        #logging config
        logging.basicConfig(filename=log_file_name,format=date_time+' %(message)s',level=logging.INFO)
        url="https://login.finvasia.com/"
        logging.info("Login URL: "+url)
        logging.info(time.time())

        #Adding Chrome options
        options = webdriver.ChromeOptions()
        options.binary_location = '/opt/google/chrome/google-chrome' #comment this line for running on local
        options.add_argument('--headless')
        options.add_argument('--no-sandbox') # this is important for running headless on ec2
        service_log_path = "{}/chromedriver.log".format("/tmp")
        service_args = ['--verbose']
        driver = webdriver.Chrome('/usr/bin/chromedriver', # give your chromedriver path here
                chrome_options=options,
                service_args=service_args,
                service_log_path=service_log_path)

        driver.get(url)
        driver.find_element_by_id("UserEmail").send_keys("######")
        driver.find_element_by_id("UserPassword").send_keys("####")
        driver.find_element_by_class_name("form_subin").click()
        statbalance=driver.find_element_by_id("statbalance").text
        statunrealizedPNL=driver.find_element_by_id("statunrealizedPNL").text
        statrealizedPNL=driver.find_element_by_id("statrealizedPNL").text
        statstockHolding=driver.find_element_by_id("statstockHolding").text
        #logging.info("statbalance>>>"+statbalance)
        #logging.info("statunrealizedPNL>>>"+statunrealizedPNL)
        #logging.info("statrealizedPNL>>>"+statrealizedPNL)
        #logging.info("statstockHolding>>>"+statstockHolding)

        prism_vals={"statbalance":statbalance,"statunrealizedPNL":statunrealizedPNL,"statrealizedPNL":statrealizedPNL,"statstockHolding":statstockHolding}
        logging.info(prism_vals)
        logging.info(time.time())
        driver.close()
        return prism_vals;

if __name__ == "__main__":
        scrapPrismFinvasia()