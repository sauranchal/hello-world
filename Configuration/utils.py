from datetime import  date, timedelta
from datetime import datetime
import calendar
import smtplib
#from ForwardVolatility.Configuration.ConfigurationUtils import ConfigurationUtils

#ConfigurationDict  = ConfigurationUtils.getInstance().getCofiguration()


#get last thursday of month
def getLastThursday(year, month):
   # Create a datetime.date for the last day of the given month
    daysInMonth = calendar.monthrange(year, month)[1]  # Returns (month, numberOfDaysInMonth)
    dt = date(year, month, daysInMonth)

    # Back up to the most recent Thursday
    offset = 4 - dt.isoweekday()
    if offset > 0: offset -= 7  # Back up one week if necessary
    dt += timedelta(offset)  # dt is now date of last Th in month
    return dt


#Get Expiry Date i.e. Last Thursday of Mid Month
def getExpiryDateMM(cmon, cyear):
    cmon = cmon+1

    # calculate mid month expiry
    if cmon > 12:
        nthu = getLastThursday(cyear+1, 1)
    else:
        nthu = getLastThursday(cyear, cmon)
    return nthu


#Get Expiry Date i.e. Last Thursday of Far Month
def getExpiryDateFM(cmon, cyear):
    cmon = cmon+2
    # calculate mid month expiry
    if cmon == 13:
        nthu = getLastThursday(cyear+1, 1)
    elif cmon == 14:
        nthu = getLastThursday(cyear + 1, 2)
    else:
        nthu = getLastThursday(cyear, cmon)
    return nthu



def getExpiryDateNM_YYMON():
    expiryDate_NM, expiryDate_MM, expiryDate_FM = getExpiryDates()
    return expiryDate_NM.strftime('%y%b').upper()

def getExpiryDateMM_YYMON():
    expiryDate_NM, expiryDate_MM, expiryDate_FM = getExpiryDates()
    return expiryDate_MM.strftime('%y%b').upper()

def getExpiryDateFM_YYMON():
    expiryDate_NM, expiryDate_MM, expiryDate_FM = getExpiryDates()
    return expiryDate_FM.strftime('%y%b').upper()

def getExpiryDateNSEFormat(expiryDate):
    return expiryDate.strftime('%d%h%Y').upper()

#get number closest to a given value
def takeClosest(num,collection):
   return min(collection,key=lambda x:abs(x-num))


#what is 5% of 20
def xPercentageOfY(percent, whole):
  return (percent * whole) / 100.0

#what is percetage is 5 of 20
def percentageIsXofY(part, whole):
  return 100 * float(part)/float(whole)

# send mail
def send_email(user, pwd, recipient, subject, body):

    FROM = user
    TO = recipient if isinstance(recipient, list) else [recipient]
    SUBJECT = subject
    TEXT = body

    # Prepare actual message
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP_SSL('smtp.mail.yahoo.com', 465)
        server.ehlo()
        #server.starttls()
        server.login(user, pwd)
        server.sendmail(FROM, TO, message)
        server.close()
        print('successfully sent the mail')
    except Exception as e:
        print("failed to send mail")
        print(e)

# CALCULATE EXPIRY DATE FOR NEXT 3 MONTHS FOR OPTION CHAIN CONTRACT
def getExpiryDates():
    todayte = date.today()
    cmon = todayte.month
    cyear = todayte.year
    #todayte = date(2019,5,31)
    #cmon, cyear = 5, 2019
    #print(todayte, cmon, cyear)
    expiryDate_NM = getLastThursday(cyear, cmon)
    expiryDate_MM = getExpiryDateMM(cmon, cyear)
    expiryDate_FM = getExpiryDateFM(cmon, cyear)
    # calculate next month expiry if thursday has been passed or delta is very less
    if expiryDate_NM.year == todayte.year and expiryDate_NM.month == todayte.month and \
            todayte > expiryDate_NM:
        print('Calculate next month expiry')

        # Handling Near Month
        if cmon != 12:
            expiryDate_NM = getLastThursday(cyear, cmon + 1)
        else:
            expiryDate_NM = getLastThursday(cyear + 1, 1)

        # Handling Near Month
        if cmon+1 == 12:
            expiryDate_MM = getLastThursday(cyear + 1, 1)
            expiryDate_FM = getLastThursday(cyear + 1, 2)
        elif cmon+1 == 13:
            expiryDate_MM = getLastThursday(cyear + 1, 2)
            expiryDate_FM = getLastThursday(cyear + 1, 3)
        elif cmon+2 == 12:
            expiryDate_MM = getLastThursday(cyear, cmon + 2)
            expiryDate_FM = getLastThursday(cyear+1, 1)
        else:
            expiryDate_MM = getLastThursday(cyear, cmon + 2)
            expiryDate_FM = getLastThursday(cyear, cmon + 3)


    # Return All 3 Dates in NSE OPTION CHAIN FORMAT
    return expiryDate_NM, expiryDate_MM, expiryDate_FM

def getExpiryDatesNSEFormat():
    expiryDate_NM, expiryDate_MM, expiryDate_FM = getExpiryDates()
    return getExpiryDateNSEFormat(expiryDate_NM), \
           getExpiryDateNSEFormat(expiryDate_MM), \
           getExpiryDateNSEFormat(expiryDate_FM)




if __name__ == "__main__":
    num = 10375
    collection = [10100,10150,10200,10250,10300]

    closestNum = takeClosest(num, collection)
    #print(closestNum)
    expiryDate_NM, expiryDate_MM, expiryDate_FM = getExpiryDatesNSEFormat()
    print(expiryDate_NM, expiryDate_MM, expiryDate_FM)
    print(getExpiryDateNM_YYMON())